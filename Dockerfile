FROM python:3.11-rc-alpine

LABEL Author="Ilia Nikitin"

ENV PYTHONBUFFERED 1

WORKDIR /baikalteam
COPY ./app/ /baikalteam

RUN pip --no-cache-dir install --upgrade pip -r requirements.txt

EXPOSE 8000

CMD ["gunicorn", "--bind", ":8000", "--workers", "4", "main:app"]
