# Based on [Official Python image](https://hub.docker.com/_/python)

- ```3.11.0a7-alpine3.15```, ```3.11-rc-alpine3.15```, ```3.11.0a7-alpine```, ```3.11-rc-alpine``` [(3.11-rc/Dockerfile)](https://github.com/docker-library/python/blob/f871d0435e7f35a693fa1b60aa159e6a1a4c6a2e/3.11-rc/alpine3.15/Dockerfile)


## Name
Hello World for Baikalteam

## Descriprtion
Simple application Hello World in Docker

## Install prerequisites

To run the docker commands without using **sudo** you must add the **docker** group to **your-user**:

```
sudo usermod -aG docker your-user
```

For now, this project has been mainly created for Unix `(Linux/MacOS)`. Perhaps it could work on Windows.

All requisites should be available for your distribution. The most important re :

* [Git](https://git-scm.com/downloads)
* [Docker](https://docs.docker.com/engine/installation/)
* [Docker Compose](https://docs.docker.com/compose/install/)


## Clone the project

To install [Git](http://git-scm.com/book/en/v2/Getting-Started-Installing-Git), download it and install following the instructions :

``` sh
    git clone https://gitlab.com/lacosteque/baikalteam.git
```

Go to the project directory :

``` sh
    cd baikalteam
```

### Project tree

```
baikalteam
├─ .dockerignore
├─ .gitlab-ci.yml
├─ Dockerfile
├─ app
│  ├─ main.py
│  ├─ requirements.txt
│  ├─ static
│  │  └─ logo.svg
│  └─ templates
│     └─ index.html
└─ docker-compose.yml
```

## Run the application

### with Docker

1. Build your image

```sh
    docker build -t hello .
```
   
2. Start

```sh
    docker run -d -p 80:8000 --name flask hello:latest
```

3. Check

After few seconds, open `http://<host>` to see the welcome page. 
The <host> is the localhost or IP of your server.

4. Stop

```sh
    docker stop flask
```

### with Docker Compose

1. Build your image

```sh
    docker-compose build
```

2. Start

```sh
    docker-compose up -d
```

3. Check

After few seconds, open `http://<host>` to see the welcome page.
The <host> is the localhost or IP of your server.

4. Stop

```sh
    docker-compose down
```

