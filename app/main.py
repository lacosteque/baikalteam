import os
from datetime import datetime
from dotenv import load_dotenv
from flask import Flask, render_template

dotenv_path = os.path.join(os.path.dirname(__file__), '.env')

if os.path.exists(dotenv_path):
    load_dotenv(dotenv_path)

app = Flask(__name__)

now = datetime.now()
timestamp = now.strftime("%d/%m/%Y %H:%M:%S")

ci_project_name = os.getenv('CI_PROJECT_NAME')
ci_project_url = os.getenv('CI_PROJECT_URL')
branch = os.getenv('CI_COMMIT_BRANCH')

context = { 'timestamp' : timestamp, 
            'ci_project_name' : ci_project_name,
            'ci_project_url' : ci_project_url,
            'branch' : branch,
           }

@app.route("/")
def index():
    return render_template('index.html', ** context)


